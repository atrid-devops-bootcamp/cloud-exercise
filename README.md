#### This project is for the Devops bootcamp exercise for

#### "Cloud Basics"

##### First step:
- packaging the application using npm pack command

##### Second step:
- creating a droplet on digital ocean
- installing nodejs and npm on the droplet

##### Third step:
- copying the tar file to the droplet using scp command
- extracting the tar file on the droplet using tar -zxvf command
- cd to the extracted folder
- npm install
- node server.js

##### Fourth step:
- Access the application using the public ip of the droplet after opening the port 3000 on the droplet using the firewall settings